﻿# Songbirds.MusicQueueUI

Important Files
- ./Songbirds.MusicQueueUI/Controllers/MusicController.cs - the code that handles all requests from <code>localhost/api/music</code>
- ./Songbirds.MusicQueueUI/Views/Home/Index.cshtml - the html file of the <code>localhost</code>
- ./Songbirds.MusicQueueUI/wwwroot/css/site.css
- ./Songbirds.MusicQueueUI/wwwroot/js/site.js 

Optional Files
- ./Songbirds.MusicQueueUI/Interfaces/Hubs/IMusicNotificationHub.cs - holds the "topics" and data that needs to be passed to the clients. add additional methods if you want more "topics" in the future

How to use
- Assuming this run in a cmd prompt, <code>dotnet run --project ./Songbirds.MusicQueueUI</code> to start the project
- Add to OBS the localhost:<port> with a set width and height
- Add the following HTTP Calls in your code (replace the <> parts)
  - http://localhost/api/music/insertatindex?title=<title>&duration=<duration>&username=<username>
    - meant to be used for "To the Top" redeem
    - inserts music based on the index given (index param is an optional param, defaults to 1 and zero indexed)
  - https://localhost/api/music/pushmusic?duration=<duration>&title=<title>&username=<username>
    - meant to be used for "Request a song" redeem
    - inserts music at the bottom of the queue/list
  - https://localhost/api/music/popmusic
    - removes the "top" or "currently playing" music
  - https://localhost/api/music/removemusic?title=<title>
    - meant for admin use
    - removes A MUSIC; searches from top to bottom