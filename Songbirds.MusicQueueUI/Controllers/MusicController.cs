﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Songbirds.MusicQueueUI.Hubs;
using Songbirds.MusicQueueUI.Interfaces.Hubs;
using Songbirds.MusicQueueUI.Models;

namespace Songbirds.MusicQueueUI.Controllers;

[ApiController]
[Route("/api/[controller]/[action]")]
public class MusicController : Controller
{
    private readonly IHubContext<MusicNotificationHub, IMusicNotificationHub> _hubContext;

    public MusicController(IHubContext<MusicNotificationHub, IMusicNotificationHub> hubContext)
    {
        _hubContext = hubContext;
    }

    [HttpGet]
    public async Task<IActionResult> PushMusic([FromQuery]Music music)
    {
        await _hubContext.Clients.All.PushMusic(music);
        return Ok();
    }

    [HttpGet]
    public async Task<IActionResult> PopMusic()
    {
        await _hubContext.Clients.All.PopMusic();
        return Ok();
    }

    [HttpGet]
    public async Task<IActionResult> InsertAtIndex([FromQuery] Music music, [FromQuery] int index = 1)
    {
        await _hubContext.Clients.All.InsertAtIndex(music, index);
        return Ok();
    }
    
    [HttpGet]
    public async Task<IActionResult> RemoveMusic([FromQuery]string title)
    {
        await _hubContext.Clients.All.RemoveMusic(title);
        return Ok();
    }
}
