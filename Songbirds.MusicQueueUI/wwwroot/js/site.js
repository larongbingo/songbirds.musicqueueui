﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

var connection = new signalR.HubConnectionBuilder().withUrl("/hubs/notifications").build();

const musicList = document.getElementById("musicList");

connection.start()
    .then(function () {
        console.log("Connection Started");
    })
    .catch(function (err) {
        return console.error(err.toString());
    });

connection.on("PushMusic", function (music) {
    console.log(music);
    const newRow = createNewRow(music.title, music.duration, music.username);
    musicList.append(newRow);
    animationStuff();
});

connection.on("PopMusic", function () {
    musicList.removeChild(musicList.children[0]);
    animationStuff();
});

connection.on("InsertAtIndex", function (music, index) {
    console.log(music, index);

    index = Number(index ?? "1");

    const newRow = createNewRow(music.title, music.duration, music.username);

    musicList.children[index].insertAdjacentElement("beforebegin", newRow);
    animationStuff()
});

connection.on("RemoveMusic", function (title) {
    console.log(title);
    for (let element of musicList.children) {
        if (element.children[0].textContent === title) {
            musicList.removeChild(element);
            animationStuff();
            return;
        }
    }
});

function createNewRow(title, duration, username) {
    const newRow = document.createElement("tr");
    const titleCol = document.createElement("td");
    const durationCol = document.createElement("td");
    const userCol = document.createElement("td");
    titleCol.textContent = title;
    durationCol.textContent = duration;
    userCol.textContent = username;

    newRow.append(titleCol);
    newRow.append(durationCol);
    newRow.append(userCol);
    return newRow;
}

// Animation stuff https://codepen.io/anon/pen/vodRyz
function animationStuff() {
    var viewHeight = window.innerHeight;
    var slider = document.querySelector(".slider");
    var time = (slider.offsetHeight * 2.0 + viewHeight * 2) / 50.0;
    slider.style.animationDuration = time + "s";
}


window.onload = animationStuff;