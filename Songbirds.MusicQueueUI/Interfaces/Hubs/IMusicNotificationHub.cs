﻿using Songbirds.MusicQueueUI.Models;

namespace Songbirds.MusicQueueUI.Interfaces.Hubs;

public interface IMusicNotificationHub
{
    Task InsertAtIndex(Music music, int index);
    Task PushMusic(Music music);
    Task PopMusic();
    Task RemoveMusic(string title);
}
