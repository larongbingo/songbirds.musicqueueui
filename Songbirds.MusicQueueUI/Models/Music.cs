﻿namespace Songbirds.MusicQueueUI.Models;

public class Music
{
    /// <summary>
    /// Title of the music
    /// </summary>
    public string Title { get; set; }
    
    /// <summary>
    /// The user who requested the music
    /// </summary>
    public string Username { get; set; }

    /// <summary>
    /// Length of the music to be played
    /// </summary>
    public TimeSpan Duration { get; set; }
}
