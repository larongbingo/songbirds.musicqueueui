﻿using Microsoft.AspNetCore.SignalR;
using Songbirds.MusicQueueUI.Interfaces.Hubs;
using Songbirds.MusicQueueUI.Models;

namespace Songbirds.MusicQueueUI.Hubs;

public class MusicNotificationHub : Hub<IMusicNotificationHub>
{
    public async Task PushMusic(Music music)
    {
        await Clients.All.PushMusic(music);
    }

    public async Task PopMusic()
    {
        await Clients.All.PopMusic();
    }
    
    public async Task RemoveMusic(string title)
    {
        await Clients.All.RemoveMusic(title);
    }

    public async Task InsertAtIndex(Music music, int index)
    {
        await Clients.All.InsertAtIndex(music, index);
    }
}
